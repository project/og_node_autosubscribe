<?php

/**
 * @file Contains the variables and defaults used by Organic Groups Node Autosubscribe.
 */

/**
 * The variable namespace for Organic Groups Node Autosubscribe.
 */
define('OG_NODE_AUTOSUBSCRIBE_NAMESPACE', 'og_node_autosubscribe__');

/**
 * Wrapper for variable_get() that uses the Organic Groups Node Autosubscribe variable registry.
 *
 * @param string $name
 *  The variable name to retrieve. Note that it will be namespaced by
 *  pre-pending OG_NODE_AUTOSUBSCRIBE_NAMESPACE, as to avoid variable collisions
 *  with other modules.
 * @param unknown $default
 *  An optional default variable to return if the variable hasn't been set
 *  yet. Note that within this module, all variables should already be set
 *  in the og_node_autosubscribe_variable_default() function.
 * @return unknown
 *  Returns the stored variable or its default.
 *
 * @see og_node_autosubscribe_variable_set()
 * @see og_node_autosubscribe_variable_del()
 * @see og_node_autosubscribe_variable_default()
 */
function og_node_autosubscribe_variable_get($name, $default = NULL) {
  // Allow for an override of the default.
  // Useful when a variable is required (like $path), but namespacing still desired.
  if (!isset($default)) {
    $default = og_node_autosubscribe_variable_default($name);
  }
  // Namespace all variables
  $variable_name = OG_NODE_AUTOSUBSCRIBE_NAMESPACE . $name;
  return variable_get($variable_name, $default);
}

/**
 * Wrapper for variable_set() that uses the Organic Groups Node Autosubscribe variable registry.
 *
 * @param string $name
 *  The variable name to set. Note that it will be namespaced by
 *  pre-pending OG_NODE_AUTOSUBSCRIBE_NAMESPACE, as to avoid variable collisions with
 *  other modules.
 * @param unknown $value
 *  The value for which to set the variable.
 * @return unknown
 *  Returns the stored variable after setting.
 *
 * @see og_node_autosubscribe_variable_get()
 * @see og_node_autosubscribe_variable_del()
 * @see og_node_autosubscribe_variable_default()
 */
function og_node_autosubscribe_variable_set($name, $value) {
  $variable_name = OG_NODE_AUTOSUBSCRIBE_NAMESPACE . $name;
  return variable_set($variable_name, $value);
}

/**
 * Wrapper for variable_del() that uses the Organic Groups Node Autosubscribe variable registry.
 *
 * @param string $name
 *  The variable name to delete. Note that it will be namespaced by
 *  pre-pending OG_NODE_AUTOSUBSCRIBE_NAMESPACE, as to avoid variable collisions with
 *  other modules.
 *
 * @see og_node_autosubscribe_variable_get()
 * @see og_node_autosubscribe_variable_set()
 * @see og_node_autosubscribe_variable_default()
 */
function og_node_autosubscribe_variable_del($name) {
  $variable_name = OG_NODE_AUTOSUBSCRIBE_NAMESPACE . $name;
  variable_del($variable_name);
}

/**
 * The default variables within the Organic Groups Node Autosubscribe namespace.
 *
 * @param string $name
 *  Optional variable name to retrieve the default. Note that it has not yet
 *  been pre-pended with the OG_NODE_AUTOSUBSCRIBE_NAMESPACE namespace at this time.
 * @return unknown
 *  The default value of this variable, if it's been set, or NULL, unless
 *  $name is NULL, in which case we return an array of all default values.
 *
 * @see og_node_autosubscribe_variable_get()
 * @see og_node_autosubscribe_variable_set()
 * @see og_node_autosubscribe_variable_del()
 */
function og_node_autosubscribe_variable_default($name = NULL) {
  static $defaults;

  if (!isset($defaults)) {
    $defaults = array();
    // Set node type defaults.
    foreach (array_keys(node_get_types()) as $type) {
      $defaults['autosubscribe_'. $type] = FALSE;
    }
  }

  if (!isset($name)) {
    return $defaults;
  }

  if (isset($defaults[$name])) {
    return $defaults[$name];
  }
}

/**
 * Return the fully namespace variable name.
 *
 * @param string $name
 *  The variable name to retrieve the namespaced name.
 * @return string
 *  The fully namespace variable name, prepended with
 *  OG_NODE_AUTOSUBSCRIBE_NAMESPACE.
 */
function og_node_autosubscribe_variable_name($name) {
  return OG_NODE_AUTOSUBSCRIBE_NAMESPACE . $name;
}

